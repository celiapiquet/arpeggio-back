package org.example.arpeggio.bootstrap;

import org.update4j.Configuration;
import org.update4j.FileMetadata;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static java.lang.String.format;

public class BootstrapConfiguration {

    public static void main(String[] args) throws IOException {
        final var applicationVersion = args[0];
        final var jobId = System.getenv("CI_JOB_ID");

        System.out.println(FileMetadata.readFrom(format("ui/target/arpeggio.ui-%s-shaded.jar", applicationVersion)).getSize());
        final var configuration = Configuration.builder()
                .baseUri(format("https://gitlab.com/celiapiquet/arpeggio-back/-/jobs/%s/artifacts/raw", jobId))
                .basePath("${user.home}/.arpeggio-back")
                .property("default.launcher.main.class", "org.example.arpeggio.ui.Main")
                .file(FileMetadata
                        .readFrom(format("ui/target/arpeggio.ui-%s-shaded.jar", applicationVersion))
                        .path("arpeggio-app.jar")
                        .classpath()
                )
                .build();

        Files.writeString(Path.of("bootstrap/target/arpeggio-app.xml"), configuration.toString());
    }
}
