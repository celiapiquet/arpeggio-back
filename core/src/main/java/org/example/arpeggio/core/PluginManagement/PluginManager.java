package org.example.arpeggio.core.PluginManagement;


import org.example.arpeggio.plugins.ExporterContract;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class PluginManager<T> {
    private final ArrayList<Class<T>> plugins = new ArrayList<>();
    private final ArrayList<T> pluginsInstance = new ArrayList<>();
    private final String pathToPluginsFolder = "file:" + System.getProperties().get("user.dir") + "/plugins/target";
    private static final Logger logger = LoggerFactory.getLogger(PluginManager.class);

    public boolean checkValidityOfADirPath(Path path){
        if (!Files.exists(path)){
            return false;
        }
        return Files.isDirectory(path);
    }

    private Map<Path, URL> getJarUrls(Path path){
        final Map<Path, URL> jarUrls = new HashMap<>();
        try {
            for (Path filePath : Files.newDirectoryStream(path)){
                if (filePath.getFileName().toString().endsWith(".jar")){
                    jarUrls.put(filePath, filePath.toUri().toURL());
                }
            }
        } catch (IOException e) {
            logger.error("IOException", e);
        }
        return jarUrls;
    }

    public void loadPluginsClass() throws Exception {
        final URI pluginURI = URI.create(this.pathToPluginsFolder);
        final Path path = Paths.get(pluginURI);
        if (!checkValidityOfADirPath(path)){
            throw new Exception("Dir path not valid");
        }
        final Map<Path, URL> jarUrls = getJarUrls(path);
        setPluginsWithJarUrls(jarUrls);
        setInstancesOfPlugins();
    }

    private void setInstancesOfPlugins() throws Exception {
        for (Class<T> tClass: this.plugins){
            if(tClass.isInterface() || Modifier.isAbstract(tClass.getModifiers())){
                continue;
            }
            this.pluginsInstance.add(tClass.getDeclaredConstructor().newInstance());
        }
    }

    @SuppressWarnings("unchecked")
    private void setPluginsWithJarUrls(Map<Path, URL> jarUrls) throws Exception {
        final URLClassLoader pluginLoader = new URLClassLoader(jarUrls.values().toArray(new URL[]{}));
        for (Path jarPath: jarUrls.keySet()){
            final File file = jarPath.toAbsolutePath().toFile();
            final JarFile jarFile;
            jarFile = new JarFile(file);
            for (Enumeration<JarEntry> entries = jarFile.entries(); entries.hasMoreElements();){
                final JarEntry entry = entries.nextElement();
                if (entry.isDirectory() || !entry.getName().endsWith(".class")){
                    continue;
                }
                String className = entry.getName().substring(0, entry.getName().length() - 6);
                className = className.replace('/','.');
                Class<?> newClass = null;
                try {
                    newClass = Class.forName(className, true, pluginLoader);
                } catch (ClassNotFoundException e) {
                    logger.error("Class not found", e);
                }
                this.plugins.add((Class<T>) newClass);
            }
        }
    }

    public ArrayList<Class<T>> getPlugins() {
        return plugins;
    }

    public ArrayList<T> getPluginsInstance() {
        return pluginsInstance;
    }
}
