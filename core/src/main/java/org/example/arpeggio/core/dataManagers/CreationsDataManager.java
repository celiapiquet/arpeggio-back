package org.example.arpeggio.core.dataManagers;

import org.example.arpeggio.core.models.Creation;
import org.example.arpeggio.core.APIManagement.APIService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

public class CreationsDataManager {
    private final ArrayList<Creation> creations;
    private final LocalDate startDate;
    private final LocalDate endDate;
    private final ChronoUnit unitUsed;
    private final LinkedHashMap<String, Integer> creationsByDate;
    private final APIService<Creation> creationAPIService;

    public CreationsDataManager(final LocalDate startDate, final LocalDate endDate, final ChronoUnit unitToUse) throws Exception {
        this.startDate = startDate;
        this.endDate = endDate;
        this.unitUsed = unitToUse;
        this.creationAPIService = new APIService<>(Creation.class);
        this.creations = this.getCreationsList();
        this.creationsByDate = this.initCreationsByDate();
    }

    private LinkedHashMap<String, Integer> initCreationsByDate() {
        LinkedHashMap<String, Integer> newCreationsByDate = initWithChronoUnits();
        String key;
        for (Creation creation: creations){
            key = getKeyFromCreation(creation);
            newCreationsByDate.putIfAbsent(key, 0);
            newCreationsByDate.put(key, newCreationsByDate.get(key) + 1);
        }
        return newCreationsByDate;
    }

    private String getKeyFromCreation(Creation creation) {
        LocalDateTime creationDate = creation.getSimpleCreatedAt();
        if (this.unitUsed == ChronoUnit.HOURS){
            return Integer.toString(creationDate.getHour());
        }
        if (this.unitUsed == ChronoUnit.MONTHS){
            return creationDate.getMonth().toString();
        }
        return creationDate.toLocalDate().toString();
    }

    private LinkedHashMap<String, Integer> initWithChronoUnits() {
        LinkedHashMap<String, Integer> emptyHashMap = new LinkedHashMap<>();
        // init with hours
        if (this.unitUsed == ChronoUnit.HOURS){
            initWithHours(emptyHashMap);
        }
        // init with days
        if (this.unitUsed == ChronoUnit.DAYS){
            initWithDays(emptyHashMap);
        }
        // init with month
        if (this.unitUsed == ChronoUnit.MONTHS){
            initWithMonths(emptyHashMap);
        }
        return emptyHashMap;
    }

    private void initWithMonths(LinkedHashMap<String, Integer> emptyHashMap) {
        for (Month month : Month.values()){
            emptyHashMap.put(month.toString(), 0);
        }
    }

    private void initWithDays(LinkedHashMap<String, Integer> emptyHashMap) {
        List<LocalDate> days = startDate.datesUntil(endDate).collect(Collectors.toList());
        days.forEach(day -> emptyHashMap.put(day.toString(), 0));
    }

    private void initWithHours(LinkedHashMap<String, Integer> emptyHashMap) {
        for (int hour = 0; hour < 24; hour ++){
            emptyHashMap.put(Integer.toString(hour), 0);
        }
    }

    private ArrayList<Creation> getCreationsList() throws Exception {
        String url = creationAPIService.getAPIUrl() + "creations?start_date_filter=" + this.startDate.toString() +
                "&end_date_filter=" + this.endDate.toString();
        return creationAPIService.getAllByUrl(url);
    }

    public ArrayList<Creation> getCreations() {
        return creations;
    }

    public LinkedHashMap<String, Integer> getCreationsByDate(){
        return creationsByDate;
    }
}
