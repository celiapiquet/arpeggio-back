package org.example.arpeggio.core.models;

import org.example.arpeggio.core.enums.ErrorStatus;
import org.example.arpeggio.core.enums.ErrorType;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Error {
    private int id;
    private int code;
    private String content;
    private ErrorType type;
    private int applicationId;
    private String stackTrace;
    private ErrorStatus status;
    private int connectionId;
    private String createdAt;
    private String deletedAt;
    private String updatedAt;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ErrorType getType() {
        return type;
    }

    public void setType(ErrorType type) {
        this.type = type;
    }

    public int getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(int applicationId) {
        this.applicationId = applicationId;
    }

    public String getStackTrace() {
        return stackTrace;
    }

    public void setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
    }

    public ErrorStatus getStatus() {
        return status;
    }

    public void setStatus(ErrorStatus status) {
        this.status = status;
    }

    public int getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(int connectionId) {
        this.connectionId = connectionId;
    }

    public LocalDateTime getCreatedAt() {
        String dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSX";
        DateTimeFormatter format = DateTimeFormatter.ofPattern(dateFormat);
        return LocalDateTime.parse(createdAt, format);
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public LocalDateTime getUpdatedAt() {
        String dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSX";
        DateTimeFormatter format = DateTimeFormatter.ofPattern(dateFormat);
        return LocalDateTime.parse(updatedAt, format);
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "Error{" +
                "id=" + id +
                ", code=" + code +
                ", content='" + content + '\'' +
                ", type=" + type +
                ", applicationId=" + applicationId +
                ", stackTrace='" + stackTrace + '\'' +
                ", status=" + status +
                ", connectionId=" + connectionId +
                ", createdAt='" + createdAt + '\'' +
                ", deletedAt='" + deletedAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                '}';
    }
}
