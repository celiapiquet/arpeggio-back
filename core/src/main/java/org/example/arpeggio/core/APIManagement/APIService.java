package org.example.arpeggio.core.APIManagement;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.arpeggio.di.ObjectGraph;
import org.example.arpeggio.di.annotations.Inject;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class APIService<T> {

    protected final APIController apiController;
    protected final Class<?>  tClass;
    private static final Logger logger = LoggerFactory.getLogger(APIService.class);


    @Inject
    public APIService(Class<T> classToUse){
        this.apiController = ObjectGraph.buildObjectGraph().get(APIController.class);
        this.tClass = classToUse;
    }

    public String getAPIUrl(){
        return apiController.getAPIUrl();
    }

    public ArrayList<T> getAllByUrl(String url) throws Exception {
        return this.getAllByUrl(url , true);
    }

    public ArrayList<T> getAllByUrl(String url, boolean useMultipleCalls) throws Exception {
        final String bodyResult = apiController.sendGetRequest(url).body();
        if (bodyResult.length() == 0){
            return null;
        }
        JSONObject firstResult = new JSONObject(bodyResult);
        List<JSONArray> allValues = new ArrayList<>();
        int totalObjectsExpected = firstResult.getInt("total");
        int actualSizeResult = firstResult.getJSONArray("values").length();
        if (actualSizeResult < totalObjectsExpected && useMultipleCalls){
            ArrayList<URI> nextUriToCall = initNextURI(url, totalObjectsExpected, actualSizeResult);
            List<String> results = apiController.sendAsyncGetRequests(nextUriToCall);
            allValues.addAll(results.stream()
                    .filter(Objects::nonNull)
                    .map(result -> new JSONObject(result).getJSONArray("values"))
                    .collect(Collectors.toList()));
        }
        allValues.add(firstResult.getJSONArray("values"));
        return allValues.stream()
                .map(this::convertToObjectList)
                .filter(Objects::nonNull)
                .flatMap(List::stream)
                .collect(Collectors.toCollection(ArrayList::new));

    }

    private ArrayList<T> convertToObjectList(JSONArray objectsArray){
        ObjectMapper mapper = new ObjectMapper();
        JavaType javaType = mapper.getTypeFactory().constructParametricType(ArrayList.class, tClass);
        try {
            return mapper.readValue(objectsArray.toString(), javaType);
        } catch (IOException e) {
            logger.error("Map impossible", e);
        }
        return null;
    }

    private ArrayList<URI> initNextURI(final String url, final int totalObjectsExpected, final int actualSizeResult) throws Exception{
        ArrayList<URI> nextUriToCall = new ArrayList<>();
        boolean hasQueryParams = url.contains("?");
        String prefix;
        if (hasQueryParams){
            prefix = "&page=";
        } else {
            prefix = "?page=";
        }
        int nbPageNeeded = totalObjectsExpected / actualSizeResult + 1;
        for (int page = 1; page < nbPageNeeded; page ++){
            nextUriToCall.add(new URI(url + prefix + page));
        }
        return nextUriToCall;
    }


    public int patchOneByUrl(String url, String operation, String newValue, String fieldToUpdate) throws Exception {
        ArrayList<HashMap<String, String>> body = new ArrayList<>();
        HashMap<String, String> patchMap = new HashMap<>();
        patchMap.put("op", operation);
        patchMap.put("value", newValue);
        patchMap.put("path", fieldToUpdate);
        body.add(patchMap);
        return apiController.sendPatchRequest(url, JSONObject.valueToString(body));
    }
}
