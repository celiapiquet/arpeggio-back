package org.example.arpeggio.core.models;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Connection {
    private int id;
    private int code;
    private String origin;
    private String destination;
    private String country;
    private String deviceType;
    private String method;
    private String body;
    private String uri;
    private String headers;
    private String type;
    private int sessionId;
    private int applicationId;
    private int connectionId;
    private String createdAt;
    private String deletedAt;
    private String updatedAt;


    @Override
    public String toString() {
        return "Connection{" +
                "id=" + id +
                ", code=" + code +
                ", origin='" + origin + '\'' +
                ", destination='" + destination + '\'' +
                ", country='" + country + '\'' +
                ", deviceType='" + deviceType + '\'' +
                ", method='" + method + '\'' +
                ", body='" + body + '\'' +
                ", uri='" + uri + '\'' +
                ", headers='" + headers + '\'' +
                ", type='" + type + '\'' +
                ", sessionId=" + sessionId +
                ", applicationId=" + applicationId +
                ", connectionId=" + connectionId +
                ", createdAt='" + createdAt + '\'' +
                ", deletedAt='" + deletedAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getHeaders() {
        return headers;
    }

    public void setHeaders(String headers) {
        this.headers = headers;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getSessionId() {
        return sessionId;
    }

    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }

    public int getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(int applicationId) {
        this.applicationId = applicationId;
    }

    public LocalDateTime getCreatedAt() {
        String dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSX";
        DateTimeFormatter format = DateTimeFormatter.ofPattern(dateFormat);
        return LocalDateTime.parse(createdAt, format);
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(int connectionId) {
        this.connectionId = connectionId;
    }
}
