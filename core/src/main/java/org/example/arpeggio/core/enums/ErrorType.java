package org.example.arpeggio.core.enums;

public enum ErrorType {
    WEB("WEB"),
    SYSTEM("SYSTEM");

    private String name = "";

    ErrorType(String name){
        this.name = name;
    }

    @Override
    public String toString() {
        return "ErrorType{" +
                "name='" + name + '\'' +
                '}';
    }
}
