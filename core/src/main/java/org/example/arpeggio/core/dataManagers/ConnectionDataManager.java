package org.example.arpeggio.core.dataManagers;


import org.example.arpeggio.core.models.Connection;
import org.example.arpeggio.core.APIManagement.APIService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

public class ConnectionDataManager {
    private final LocalDate startDate;
    private final LocalDate endDate;
    private final String onePeriodUnit;
    private final boolean needRegion;
    private final List<Connection> connections;
    private final ArrayList<String> regions;
    private final int nbRegions;
    private final String connectionType;
    private final String deviceChoice;
    private final LinkedHashMap<String, LinkedHashMap<String, ArrayList<Connection>>> dataGrid;
    private final LinkedHashMap<String, LinkedHashMap<String, ArrayList<Connection>>> dataChart;

    public ConnectionDataManager(LocalDate startDate, LocalDate endDate, boolean needRegion, String connectionType,
                                 String deviceChoice) throws Exception {

        this.connectionType = connectionType;
        this.deviceChoice = deviceChoice;

        this.startDate = startDate;
        this.endDate = endDate;
        long intervalDays = ChronoUnit.DAYS.between(startDate, endDate);
        if (intervalDays == 1){
            this.onePeriodUnit = "Hours";
        } else {
            this.onePeriodUnit = "Days";
        }
        this.needRegion = needRegion;
        this.connections = this.getConnectionsList(connectionType, deviceChoice);
        this.regions = new ArrayList<>();
        this.initRegions();
        this.nbRegions = regions.size();
        this.dataGrid = this.createDataGrid();
        this.dataChart = this.createDataChart();
    }

    @Override
    public String toString() {
        return "Connections from " + startDate +
                " to " + endDate +
                ". Detailed region : " + needRegion +
                ". Device selected : " + deviceChoice +
                ". Type of connections : " + connectionType;
    }

    private void initRegions() {
        if (!this.needRegion){
            this.regions.add("All Region");
            return;
        }
        for (Connection c : this.connections){
            String connectionRegion = c.getCountry();
            if (connectionRegion == null){
                connectionRegion = "unknown";
            }
            if (!this.regions.contains(connectionRegion)){
                this.regions.add(connectionRegion);
            }
        }
    }

    private LinkedHashMap<String, LinkedHashMap<String, ArrayList<Connection>>> createDataGrid() {
        LinkedHashMap<String, LinkedHashMap<String, ArrayList<Connection>>> data = initDataByDay();
        connections.forEach((c) -> {
            String region = this.getConnectionRegion(c);
            LocalDateTime connectionDate = c.getCreatedAt();
            String date = connectionDate.toLocalDate().toString();
            String hour = Integer.toString(connectionDate.getHour());
            if (data.containsKey(date)){
                data.get(date).get(region).add(c);
            } else if (data.containsKey(hour)){
                data.get(hour).get(region).add(c);
            }
        });
        return data;
    }

    private String getConnectionRegion(Connection c){
        String region = c.getCountry();
        if (region == null){
            region = "unknown";
        }
        if (!this.needRegion){
            region = "All Region";
        }
        return region;
    }

    private LinkedHashMap<String, LinkedHashMap<String, ArrayList<Connection>>> createDataChart() {
        LinkedHashMap<String, LinkedHashMap<String, ArrayList<Connection>>> data = initDataByRegion();
        connections.forEach(connection -> {
            String region = this.getConnectionRegion(connection);
            LocalDateTime connectionDate = connection.getCreatedAt();
            String date = connectionDate.toLocalDate().toString();
            String hour = Integer.toString(connectionDate.getHour());
            if(data.get(region).containsKey(date)){
                data.get(region).get(date).add(connection);
            } else if (data.get(region).containsKey(hour)) {
                data.get(region).get(hour).add(connection);
            }
        });
        return data;
    }

    private List<Connection> getConnectionsList(String connectionType, String deviceChoice) throws Exception {
        APIService<Connection> connectionAPIService = new APIService<>(Connection.class);
        String url = connectionAPIService.getAPIUrl() + "connections?type_filter=" + connectionType + "&start_date_filter="
                + this.startDate + "&end_date_filter=" + this.endDate;
        if (!deviceChoice.equals("All")){
            url = url + "&device_type_filter=" + deviceChoice;
        }
        return connectionAPIService.getAllByUrl(url);
    }

    private LinkedHashMap<String, LinkedHashMap<String, ArrayList<Connection>>> initDataByRegion() {
        LinkedHashMap<String, LinkedHashMap<String, ArrayList<Connection>>> regionMap = new LinkedHashMap<>();
        for (int i = 0; i < this.nbRegions; i++){
            regionMap.put(this.regions.get(i), this.initDateMap());
        }
        return regionMap;
    }

    private LinkedHashMap<String, ArrayList<Connection>> initDateMap() {
        LinkedHashMap<String, ArrayList<Connection>> dateMap = new LinkedHashMap<>();
        if (this.onePeriodUnit.equals("Hours")){
            for (int i = 0; i < 24; i ++){
                dateMap.put(Integer.toString(i), new ArrayList<>());
            }
        } else {
            startDate.datesUntil(endDate).collect(Collectors.toList()).forEach((c) -> {
                dateMap.put(c.toString(), new ArrayList<>());
            });
        }
        return dateMap;
    }

    private LinkedHashMap<String, LinkedHashMap<String, ArrayList<Connection>>> initDataByDay() {
        LinkedHashMap<String, LinkedHashMap<String, ArrayList<Connection>>> dataEmpty = new LinkedHashMap<>();
        if (this.onePeriodUnit.equals("Hours")){
            for (int i = 0; i < 24; i ++){
                dataEmpty.put(Integer.toString(i), this.initRegionMap());
            }
        } else {
            startDate.datesUntil(endDate).collect(Collectors.toList()).forEach((c) -> {
                dataEmpty.put(c.toString(), this.initRegionMap());
            });
        }
        return dataEmpty;
    }

    private LinkedHashMap<String, ArrayList<Connection>> initRegionMap() {
        LinkedHashMap<String, ArrayList<Connection>> regionMap = new LinkedHashMap<>();
        for (String region : this.regions){
            regionMap.put(region, new ArrayList<>());
        }
        return regionMap;
    }

    public LinkedHashMap<String, LinkedHashMap<String, ArrayList<Connection>>> getDataGrid() {
        return dataGrid;
    }

    public LinkedHashMap<String, LinkedHashMap<String, ArrayList<Connection>>> getDataChart() {
        return dataChart;
    }

    public String getOnePeriodUnit() {
        return onePeriodUnit;
    }

    public int getNbRegions() {
        return nbRegions;
    }

    public List<Connection> getConnections() {
        return this.connections;
    }
}
