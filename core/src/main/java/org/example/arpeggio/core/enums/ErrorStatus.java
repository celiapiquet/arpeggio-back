package org.example.arpeggio.core.enums;

public enum ErrorStatus {
    CLOSED("CLOSED"),
    OPEN("OPEN");

    private String name = "";

    ErrorStatus(String name){
        this.name = name;
    }

    public static boolean hasValue(String status) {
        for (ErrorStatus errorStatus : ErrorStatus.values()){
            if (errorStatus.name.equals(status)){
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "ErrorStatus{" +
                "name='" + name + '\'' +
                '}';
    }
}
