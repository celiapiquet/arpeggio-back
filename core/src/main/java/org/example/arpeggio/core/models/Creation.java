package org.example.arpeggio.core.models;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Creation {
    private String compositionPath;
    private String compositionVisibility;
    private int id;
    private String name;
    private int projectId;
    private int instrumentId;
    private String status;
    private String publicationTime;
    private String createdAt;
    private String deletedAt;
    private String updatedAt;

    public String getCompositionPath() {
        return compositionPath;
    }

    public void setCompositionPath(String compositionPath) {
        this.compositionPath = compositionPath;
    }

    public String getCompositionVisibility() {
        return compositionVisibility;
    }

    public void setCompositionVisibility(String compositionVisibility) {
        this.compositionVisibility = compositionVisibility;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public int getInstrumentId() {
        return instrumentId;
    }

    public void setInstrumentId(int instrumentId) {
        this.instrumentId = instrumentId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPublicationTime() {
        return publicationTime;
    }

    public void setPublicationTime(String publicationTime) {
        this.publicationTime = publicationTime;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public LocalDateTime getSimpleCreatedAt(){
        String dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSX";
        DateTimeFormatter format = DateTimeFormatter.ofPattern(dateFormat);
        return LocalDateTime.parse(createdAt, format);
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "Creation{" +
                "compositionPath='" + compositionPath + '\'' +
                ", compositionVisibility='" + compositionVisibility + '\'' +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", projectId=" + projectId +
                ", instrumentId=" + instrumentId +
                ", status='" + status + '\'' +
                ", publicationTime='" + publicationTime + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", deletedAt='" + deletedAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                '}';
    }
}
