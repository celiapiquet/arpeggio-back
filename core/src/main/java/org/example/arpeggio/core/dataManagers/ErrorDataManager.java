package org.example.arpeggio.core.dataManagers;

import org.example.arpeggio.core.enums.ErrorStatus;
import org.example.arpeggio.core.models.Error;
import org.example.arpeggio.core.APIManagement.APIService;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class ErrorDataManager {

    private final ErrorStatus statusNeeded;
    private final APIService<Error> errorAPIService;
    private final ArrayList<Error> errors;
    private final LocalDate dateOfTickets;

    public ErrorDataManager(final ErrorStatus statusNeeded, final LocalDate dateOfTickets) throws Exception {
        this.dateOfTickets = dateOfTickets;
        this.statusNeeded = statusNeeded;
        this.errorAPIService = new APIService<>(Error.class);
        this.errors = this.getErrorsList(dateOfTickets);
    }

    private ArrayList<Error> getErrorsList(LocalDate dateOfTickets) throws Exception {
        final String startDate = dateOfTickets.toString();
        final String endDate = dateOfTickets.plus(1, ChronoUnit.DAYS).toString();
        String url = errorAPIService.getAPIUrl() + "errors?start_date_filter=" + startDate + "&end_date_filter=" + endDate;
        if (statusNeeded != null){
            url = url + "&status_filter=" + statusNeeded.name();
        }
        return errorAPIService.getAllByUrl(url, false);
    }

    public ArrayList<Error> getErrors() {
        return errors;
    }

    public int updateStatus(int errorId, ErrorStatus status) throws Exception {
        this.errors.stream().filter(error -> error.getId() == errorId).forEach(error -> error.setStatus(status));
        final String url = errorAPIService.getAPIUrl() + "errors/" + errorId;
        return this.errorAPIService.patchOneByUrl(url, "replace", status.name(), "/status");
    }
}
