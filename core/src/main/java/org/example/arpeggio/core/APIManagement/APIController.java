package org.example.arpeggio.core.APIManagement;

import org.example.arpeggio.di.annotations.Inject;
import org.example.arpeggio.di.annotations.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import static java.net.http.HttpResponse.BodyHandlers.ofString;

@Singleton
public class APIController {

    private final HttpClient client;
    private final String APIUrl;
    private static final Logger logger = LoggerFactory.getLogger(APIController.class);

    @Inject
    public APIController() {
        this.client = HttpClient.newHttpClient();
        this.APIUrl = "http://localhost:5000/api/";
    }


    public List<String> sendAsyncGetRequests(List<URI> uris){
        List<CompletableFuture<String>> results = uris.stream()
                .map(uri -> client
                        .sendAsync(
                                HttpRequest.newBuilder(uri).GET().build(),
                                ofString())
                        .thenApply(HttpResponse::body))
                .collect(Collectors.toList());

        return results.stream().map(r -> {
            try {
                return r.get();
            } catch (Exception e) {
                logger.error("Error ", e);
            }
            return null;
        }).collect(Collectors.toList());
    }

    public HttpResponse<String> sendGetRequest(String targetedUrl) throws Exception {
        HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(targetedUrl))
                    .GET()
                    .header("accept", "application/json")
                    .build();
        return sendSyncRequest(request);
    }

    public int sendPatchRequest(String url, String body) throws Exception {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .method("PATCH", HttpRequest.BodyPublishers.ofString(body))
                .header("Content-Type", "application/json")
                .build();
        return sendSyncRequest(request).statusCode();
    }

    public HttpResponse<String> sendPostRequest(String targetedUrl, String body) throws Exception {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(targetedUrl))
                .POST(HttpRequest.BodyPublishers.ofString(body))
                .build();
        return sendSyncRequest(request);
    }

    public HttpResponse<String> sendSyncRequest(HttpRequest request) throws Exception {
        return this.client.send(request, HttpResponse.BodyHandlers.ofString());
    }

    public String getAPIUrl() {
        return APIUrl;
    }
}
