package org.example.arpeggio.di;

import org.example.arpeggio.di.annotations.Inject;
import org.example.arpeggio.di.annotations.Singleton;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

public class Linker {
    private final Map<Class<?>, Factory<?>> factories = new HashMap<>();
    private final Map<Class<?>, Factory<?>> linkedFactories = new HashMap<>();

    public <T> void install(Class<T> key, Factory<T> factory){
        factories.put(key, factory);
    }

    @SuppressWarnings("unchecked")
    public <T> Factory<T> factoryFor(Class<T> key){
        Factory<?> factory = linkedFactories.get(key);
        if (factory == null){
            factory = loadFactory(key);
            factory.link(this);
            linkedFactories.put(key, factory);
        }
        return (Factory<T>) factory;
    }

    private <T> Factory<?> loadFactory(Class<T> key) {
        Factory<?> factory = factories.get(key);
        if (factory != null){
            return factory;
        }
        Constructor<T> constructor = findAtInjectConstructor(key);
        if (constructor != null){
            factory = new ReflectiveFactory<>(constructor);
            if (key.isAnnotationPresent(Singleton.class)){
                factory = SingletonFactory.of(factory);
            }
            return factory;
        }
        throw new IllegalStateException("No factory for " + key);
    }

    @SuppressWarnings("unchecked")
    private <T> Constructor<T> findAtInjectConstructor(Class<T> type) {
        for (Constructor<?> constructor : type.getConstructors()) {
            if (constructor.isAnnotationPresent(Inject.class)){
                return (Constructor<T>) constructor;
            }
        }
        return null;
    }

}
