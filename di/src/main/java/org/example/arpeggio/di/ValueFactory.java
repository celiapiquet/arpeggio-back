package org.example.arpeggio.di;

public class ValueFactory<T> implements Factory<T> { //always return the same instance

    public static <T> Factory<T> of(T instance){
        return new ValueFactory<>(instance);
    }

    private final T value;

    private ValueFactory(T value){
        this.value = value;
    }

    @Override
    public T get() {
        return value;
    }
}
