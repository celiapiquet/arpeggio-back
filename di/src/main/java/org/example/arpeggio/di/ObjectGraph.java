package org.example.arpeggio.di;

import java.io.PrintStream;

public class ObjectGraph {
    private final Linker linker;
    private static ObjectGraph instance;

    private ObjectGraph(Linker linker){
        this.linker = linker;
    }

    public <T> boolean isValid(Class<T> key){
        return linker.factoryFor(key) != null;
    }

    public <T> T get(Class<T> key){
        Factory<T> factory = linker.factoryFor(key);
        return factory.get();
    }

    public static ObjectGraph buildObjectGraph(){
        if (instance == null){
            Linker linker = new Linker();
//            linker.install(PrintStream.class, () -> System.out);
            instance = new ObjectGraph(linker);
        }
        return instance;
    }
}
