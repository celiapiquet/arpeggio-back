package org.example.arpeggio.di;

@FunctionalInterface
public interface Factory<T>{

    public abstract T get();

    default void link(Linker linker){};
}
