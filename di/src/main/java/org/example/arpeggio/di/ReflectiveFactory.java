package org.example.arpeggio.di;

import java.lang.reflect.Constructor;
import java.util.ArrayList;

public class ReflectiveFactory<T> implements Factory<T> {

    private final Constructor<T> constructor;
    private final ArrayList<Factory<?>> factories = new ArrayList<>();

    public ReflectiveFactory(Constructor<T> constructor) {
        this.constructor = constructor;
    }

    @Override
    public void link(Linker linker) {
        for (Class<?> parameterType : constructor.getParameterTypes()) {
            System.out.println(parameterType.getCanonicalName());
            factories.add(linker.factoryFor(parameterType));
        }
    }

    @Override
    public T get() {
        Object[] dependencies = factories.stream().map(Factory::get).toArray();
        try {
            return constructor.newInstance(dependencies);
        } catch (Exception e) {
            throw new  RuntimeException(e);
        }
    }
}
