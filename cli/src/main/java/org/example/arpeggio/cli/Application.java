package org.example.arpeggio.cli;

import org.example.arpeggio.cli.runners.*;
import org.example.arpeggio.cli.utils.ScannerUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Application {
    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        logger.info("Application CLI started");

        while (true) {
            System.out.println("Actions :");
            System.out.println("1. Get connections");
            System.out.println("2. Get creations");
            System.out.println("3. Get errors tickets");
            System.out.println("4. Download connections Report (you need some plugins)");
            System.out.println("5. Exit");
            int action = ScannerUtils.getIntFromUser("Please choose an action : ", 1,5);
            if (action == 5){
                break;
            }
            performAction(action);

        }
        logger.info("Application CLI ended");
    }

    private static void performAction(int actionChoice) {
        BasicRunner runner = null;
        if (actionChoice == 1){
             runner = new ConnectionsRunner();
        } else if (actionChoice == 2) {
            runner = new CreationsRunner();
        } else if (actionChoice == 3) {
            runner = new ErrorTicketsRunner();
        } else {
            try {
                runner = new ReportsRunner();
            } catch (Exception e) {
                System.out.println("ERROR : please contact the support");
                logger.error("ReportsRunner init failed");
                System.exit(1);
            }
        }
        runner.start();
    }
}
