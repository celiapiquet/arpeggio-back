package org.example.arpeggio.cli.runners;

import org.example.arpeggio.cli.utils.ConnectionsManagerUtils;
import org.example.arpeggio.cli.utils.ScannerUtils;
import org.example.arpeggio.core.PluginManagement.PluginManager;
import org.example.arpeggio.core.dataManagers.ConnectionDataManager;
import org.example.arpeggio.plugins.ExporterContract;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class ReportsRunner implements BasicRunner {

    private final PluginManager<ExporterContract> pluginManager;
    private final ConnectionDataManager connectionDataManager;
    private static final Logger logger = LoggerFactory.getLogger(ReportsRunner.class);

    public ReportsRunner() throws Exception {
        this.pluginManager = new PluginManager<>();
        this.connectionDataManager = ConnectionsManagerUtils.getNewConnectionManager();
    }


    @Override
    public void start() {
        try {
            pluginManager.loadPluginsClass();
            final ArrayList<ExporterContract> plugins = pluginManager.getPluginsInstance();
            displayPluginsChoices(plugins);
            if (plugins.size() == 0){
                return;
            }
            final int choice = ScannerUtils.getIntFromUser("Which plugin to use ? ", 1, plugins.size());
            final JSONArray data = new JSONArray(connectionDataManager.getConnections());
            final String title = connectionDataManager.toString();
            plugins.get(choice-1).uploadFile(data, title);
        } catch (Exception e) {
            System.out.println("ERROR: please contact the support");
            logger.error("ERROR: while creating ConnectionDataManager");
            System.exit(1);}

    }

    private void displayPluginsChoices(ArrayList<ExporterContract> plugins) {
        System.out.println("Plugins available :");
        if (plugins.size() == 0){
            System.out.println("No plugins found");
            return;
        }
        for (int i=0; i< plugins.size(); i++){
            System.out.println((i + 1) + ". " + plugins.get(i));
        }
    }
}
