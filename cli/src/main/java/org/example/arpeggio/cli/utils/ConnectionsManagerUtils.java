package org.example.arpeggio.cli.utils;

import org.example.arpeggio.core.dataManagers.ConnectionDataManager;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class ConnectionsManagerUtils {

    public static ConnectionDataManager getNewConnectionManager() throws Exception {
        final LocalDate startDate = ScannerUtils.getLocalDateFromUser("From what date must the report start ?",
                null , LocalDate.now().plusDays(1));
        final LocalDate endDate = ScannerUtils.getLocalDateFromUser("To what start must the report end ?",
                startDate, null);;
        final boolean needRegions = askNeedRegions();
        final String connectionType = askConnectionType();
        final String deviceChoice = askDeviceChoice();
        return new ConnectionDataManager(startDate, endDate, needRegions, connectionType, deviceChoice);
    }

    private static String askDeviceChoice() {
        final HashMap<Integer, String> devices = new HashMap<>();
        devices.put(1, "All");
        devices.put(2, "Desktop");
        devices.put(3, "Smartphone");
        devices.put(4, "Tablet");
        devices.put(5, "Other (Tv, Bot, Car, ...)");
        System.out.println("Please choose a device");
        for (Map.Entry<Integer, String> device : devices.entrySet()){
            System.out.println(device.getKey() + ". " + device.getValue());
        }
        final int action = ScannerUtils.getIntFromUser("-> ", 1, 5);
        return devices.get(action);
    }

    private static String askConnectionType() {
        final HashMap<Integer, String> connectionTypes = new HashMap<>();
        connectionTypes.put(1, "Response");
        connectionTypes.put(2, "Request");
        System.out.println("Please choose a connection type");
        for (HashMap.Entry<Integer, String> connectionType : connectionTypes.entrySet()){
            System.out.println(connectionType.getKey() + ". " + connectionType.getValue());
        }
        final int action = ScannerUtils.getIntFromUser("-> ", 1, 2);
        return connectionTypes.get(action);
    }

    private static boolean askNeedRegions() {
        return ScannerUtils.getBooleanFromUser("Do you want to have regions detailed ?");
    }
}
