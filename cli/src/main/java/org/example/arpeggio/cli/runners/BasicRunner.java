package org.example.arpeggio.cli.runners;

public interface BasicRunner {
    public abstract void start();
}
