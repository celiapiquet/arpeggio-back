package org.example.arpeggio.cli.runners;

import org.example.arpeggio.cli.utils.ScannerUtils;
import org.example.arpeggio.core.dataManagers.CreationsDataManager;
import org.example.arpeggio.core.models.Creation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class CreationsRunner implements BasicRunner {

    private final LocalDate startDate;
    private final LocalDate endDate;
    private final ChronoUnit unitToUse;
    private static final Logger logger = LoggerFactory.getLogger(CreationsRunner.class);


    public CreationsRunner() {
        this.startDate = ScannerUtils.getLocalDateFromUser("From what date must the report start ?", null , LocalDate.now().plusDays(1));
        this.endDate = ScannerUtils.getLocalDateFromUser("To what start must the report end ?", this.startDate, null);;
        this.unitToUse = ChronoUnit.DAYS;

    }

    @Override
    public void start() {
        try {
            final CreationsDataManager creationsDataManager = new CreationsDataManager(startDate, endDate, unitToUse);
            if (creationsDataManager.getCreations().size() == 0){
                System.out.println("No creations found");
            }
            for (Creation creation : creationsDataManager.getCreations()){
                System.out.println(creation);
            }
        } catch (Exception e) {
            System.out.println("ERROR: please contact the support");
            logger.error("ERROR: while creating ConnectionDataManager");
            System.exit(1);
        }
    }
}
