package org.example.arpeggio.cli.runners;

import org.example.arpeggio.cli.utils.ConnectionsManagerUtils;
import org.example.arpeggio.core.dataManagers.ConnectionDataManager;
import org.example.arpeggio.core.models.Connection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConnectionsRunner implements BasicRunner {

    private static final Logger logger = LoggerFactory.getLogger(ConnectionsRunner.class);

    public ConnectionsRunner() {
    }


    @Override
    public void start() {
        System.out.println("Please wait ...");
        try {
            final ConnectionDataManager connectionDataManager = ConnectionsManagerUtils.getNewConnectionManager();
            if (connectionDataManager.getConnections().size() == 0){
                System.out.println("No connections found");
            }
            for (Connection connection: connectionDataManager.getConnections()){
                System.out.println(connection);
            }
        } catch (Exception e) {
            System.out.println("ERROR: please contact the support");
            logger.error("ERROR: while creating ConnectionDataManager");
            System.exit(1);
        }
    }
}
