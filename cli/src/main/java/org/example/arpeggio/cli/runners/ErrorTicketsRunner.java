package org.example.arpeggio.cli.runners;

import org.example.arpeggio.cli.utils.ScannerUtils;
import org.example.arpeggio.core.dataManagers.ErrorDataManager;
import org.example.arpeggio.core.enums.ErrorStatus;
import org.example.arpeggio.core.models.Error;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.HashMap;

public class ErrorTicketsRunner implements BasicRunner {

    private final ErrorStatus statusNeeded;
    private final LocalDate dateOfTickets;
    private static final Logger logger = LoggerFactory.getLogger(ErrorTicketsRunner.class);

    public ErrorTicketsRunner(){
        this.statusNeeded = askStatus();
        this.dateOfTickets = ScannerUtils.getLocalDateFromUser("Which date to pick the tickets ? ", null, LocalDate.now());
    }


    private ErrorStatus askStatus() {
        final HashMap<Integer, ErrorStatus> status = new HashMap<>();
        final ErrorStatus[] errorStatus = ErrorStatus.values();
        System.out.println("Status :");
        for (int i=0; i < errorStatus.length; i++){
            status.put((i+1), errorStatus[i]);
            System.out.println((i+1) + ". " + errorStatus[i].name());
        }
        final int action = ScannerUtils.getIntFromUser("What status do you want : ", 1, errorStatus.length);
        return status.get(action);
    }

    @Override
    public void start() {
        try {
            final ErrorDataManager errorDataManager = new ErrorDataManager(statusNeeded, dateOfTickets);
            if (errorDataManager.getErrors().size() == 0){
                System.out.println("No tickets found");
            } else {
                for (Error error: errorDataManager.getErrors()){
                    System.out.println(error);
                }
            }
        } catch (Exception e) {
            System.out.println("ERROR: please contact the support");
            logger.error("ERROR: while creating ConnectionDataManager");
            System.exit(1);
        }

    }
}
