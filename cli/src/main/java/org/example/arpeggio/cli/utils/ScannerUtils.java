package org.example.arpeggio.cli.utils;

import javax.swing.text.DateFormatter;
import java.text.DateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

public class ScannerUtils {
    private static final Scanner scanner = new Scanner(System.in);

    public static int getIntFromUser(String question, int min, int max){
        if (max - min < 0) {
            return min;
        }
        while (true) {
            System.out.print(question);
            try {
                int choice = Integer.parseInt(scanner.nextLine()); // nextInt not very better
                if (choice < min || choice > max ){
                    System.out.println("EEROR : Choice must be a number between 1 and 5");
                }
                else {
                    return choice;
                }

            } catch (NumberFormatException e){
                System.out.println("ERROR : Your choice must be a number");
            }
        }
    }

    public static boolean getBooleanFromUser(String question) {
        System.out.println(question);
        String answer;
        do {
            System.out.print("(yes/no) -> ");
            answer = scanner.nextLine();
        } while (!answer.equals("yes") && !answer.equals("no"));
        return answer.equals("yes");
    }

    public static LocalDate getLocalDateFromUser(String question, LocalDate limitMin, LocalDate limitMax) {
        System.out.println(question);
        System.out.println("Format : dd-mm-yyyy (05-01-2020 for example)");
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        String userInput;
        while (true) {
            try {
                System.out.print("-> ");
                userInput = scanner.nextLine();
                LocalDate date = LocalDate.parse(userInput, dateFormat);
                if (dateIsValid(date, limitMin, limitMax)){
                    return date;
                }
            } catch (DateTimeParseException e){
                System.out.println("ERROR : Please respect the format dd-MM-yyyy (05-01-2020 for example)");
            }
        }
    }

    private static boolean dateIsValid(LocalDate date, LocalDate limitMin, LocalDate limitMax) {
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        if (limitMax == null && limitMin == null){
            return true;
        }
        if (limitMax != null && limitMin != null){
            if(date.compareTo(limitMin) >= 0 && date.compareTo(limitMax) <= 0){
                return true;
            } else {
                System.out.println("Error : date must be between " + limitMin.format(dateFormat) + " and " + limitMax.format(dateFormat));
                return false;
            }
        }
        if (limitMax != null){
            if (date.compareTo(limitMax) <= 0){
                return true;
            } else {
                System.out.println("Error : date must be before " + limitMax.format(dateFormat));
            }
        }
        if (limitMin != null){
            if (date.compareTo(limitMin) >= 0){
                return true;
            } else {
                System.out.println("Error : date must be after " + limitMin.format(dateFormat));
            }
        }
        return false;
    }
}
