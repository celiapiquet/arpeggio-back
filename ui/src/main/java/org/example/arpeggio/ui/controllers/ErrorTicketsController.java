package org.example.arpeggio.ui.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import org.example.arpeggio.core.enums.ErrorStatus;
import org.example.arpeggio.core.models.Error;
import org.example.arpeggio.core.dataManagers.ErrorDataManager;
import org.example.arpeggio.ui.Router;
import org.example.arpeggio.ui.services.ErrorTicketsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;

import static org.example.arpeggio.core.enums.ErrorStatus.*;

public class ErrorTicketsController {
    private Router router;
    private ErrorTicketsService errorTicketsService;
    private ErrorDataManager errorDataManager;
    private static final Logger logger = LoggerFactory.getLogger(ConnectionsReportController.class);

    @FXML
    private MenuButton statusChoice;
    @FXML
    private DatePicker datePickerForTickets;
    @FXML
    private VBox ticketsPlace;
    @FXML
    private VBox fullTicketPlace;


    @FXML
    public void initialize() {

        this.errorTicketsService = new ErrorTicketsService(statusChoice.textProperty(), datePickerForTickets.valueProperty());

        this.errorTicketsService.setOnSucceeded(event -> {
            this.errorDataManager = (ErrorDataManager) event.getSource().getValue();
            this.initListOfErrors();
        });

        this.errorTicketsService.setOnFailed(event -> {
          ErrorController.sendAlert(event.getSource().getException().toString());
          logger.error("Error tickets service", event.getSource().getException());
        });

        this.datePickerForTickets.setValue(LocalDate.now());
        this.datePickerForTickets.setOnAction(actionEvent -> {
            this.errorTicketsService.reset();
            this.errorTicketsService.start();
        });

        statusChoice.getItems().forEach(item -> {
            item.setOnAction(actionEvent -> {
                this.errorTicketsService.reset();
                statusChoice.setText(item.getText());
                this.errorTicketsService.start();
            });
        });
        this.statusChoice.setText("ALL");
        this.errorTicketsService.start();
    }

    private void initListOfErrors() {
        this.ticketsPlace.getChildren().clear(); // clear the list of errors
        for (Error error : errorDataManager.getErrors()){
            AnchorPane errorTicket = getErrorTicketFromError(error);
            this.ticketsPlace.getChildren().add(errorTicket);
        }
        if (errorDataManager.getErrors().size() == 0){
            this.ticketsPlace.getChildren().add(new Label("No error this day"));
        }
    }

    private HBox getHeaderOfTicket(Error error){
        HBox headerOfTicket = new HBox();
        Text ticketId = new Text("(" + error.getId() + ")");
        Text ticketDateFormated = new Text(error.getCreatedAt().toLocalDate().toString() + " à " +
                error.getCreatedAt().getHour() + "h" + error.getCreatedAt().getMinute());
        ticketId.setStyle("-fx-font-weight: bold");
        ticketDateFormated.setStyle("-fx-font-weight: bold");
        headerOfTicket.getChildren().addAll(ticketId, ticketDateFormated);
        return headerOfTicket;
    }

    private AnchorPane getErrorTicketFromError(Error error) {
        AnchorPane fullTicket = new AnchorPane();
        VBox ticketValues = getTicket(error);
        Button buttonStatus = getNewButtonForTicket(error);

        fullTicket.getStyleClass().add("errorTicket");
        fullTicket.idProperty().setValue("errorTicket" + error.getId());
        fullTicket.getChildren().add(ticketValues);
        fullTicket.getChildren().add(buttonStatus);

        fullTicket.setOnMouseClicked(event -> {
            this.ticketsPlace.getChildren().forEach(ticket -> ticket.getStyleClass().remove("ticketSelected"));
            displayFullTicket(error);
            fullTicket.getStyleClass().add("ticketSelected");
        });

        return fullTicket;
    }

    private VBox getTicket(Error error) {
        VBox ticketValues = new VBox();
        HBox headerOfTicket = getHeaderOfTicket(error);
        HBox resume = getResumeOfTicket(error);

        ticketValues.getChildren().add(headerOfTicket);
        ticketValues.getChildren().add(resume);

        AnchorPane.setTopAnchor(ticketValues,0.0);
        AnchorPane.setLeftAnchor(ticketValues,0.0);
        AnchorPane.setBottomAnchor(ticketValues,0.0);

        return ticketValues;
    }

    private void displayFullTicket(Error error) {
        this.fullTicketPlace.getChildren().clear();
        HBox header = getHeaderOfTicket(error); // createdAt + id
        HBox resume = getResumeOfTicket(error); // code + last update
        VBox fullContent = getFullContentOfTicket(error); // content + type + application id + stack trace + connection id
        this.fullTicketPlace.getChildren().addAll(header, resume, fullContent);
    }

    private VBox getFullContentOfTicket(Error error) {
        HBox contentInfo = new HBox(new Label("Content : "), new Label(error.getContent()));
        HBox littleInfos = new HBox(new Label("Error Type : "), new Label(error.getType().name()),
                new Label(" Application Id : "), new Label(Integer.toString(error.getApplicationId())),
                new Label(" Connection Id : "), new Label(Integer.toString(error.getConnectionId())));
        TextArea stacktrace = new TextArea(error.getStackTrace());
        stacktrace.setEditable(false);
        VBox stackTraceInfo = new VBox(new Label("Stack Trace : "), stacktrace);
        return new VBox(contentInfo, littleInfos, stackTraceInfo);
    }

    private HBox getResumeOfTicket(Error error) {
        Text ticketCode = new Text("Code : " + error.getCode());
        Text ticketLastUpdate = new Text(" Last Update : " + error.getUpdatedAt().toLocalDate().toString());
        ticketLastUpdate.setStyle("-fx-font-style: italic");
        HBox resume = new HBox(ticketCode, ticketLastUpdate);
        return resume;
    }


    private Button getNewButtonForTicket(Error error){
        ErrorStatus status = error.getStatus();
        Button buttonStatus = new Button(status.name());
        if (status.compareTo(OPEN) == 0){
            buttonStatus.getStyleClass().add("buttonErrorStatusOpen");
        } else {
            buttonStatus.getStyleClass().add("buttonErrorStatusClosed");
        }
        buttonStatus.idProperty().setValue("buttonStatus_" + error.getId());
        setActionOfStatusButton(buttonStatus, error);

        AnchorPane.setTopAnchor(buttonStatus,0.0);
        AnchorPane.setRightAnchor(buttonStatus,0.0);
        AnchorPane.setBottomAnchor(buttonStatus,0.0);
        return buttonStatus;
    }

    private void setActionOfStatusButton(Button buttonStatus, Error error) {
        buttonStatus.setOnAction(actionEvent -> {
            try {
                if (error.getStatus().compareTo(CLOSED) == 0) {
                    if (errorDataManager.updateStatus(error.getId(), OPEN) != 200) {
                        ErrorController.sendAlert("Update status failed");
                        logger.error("Update Status of error " + error.getId() + " to OPEN failed");
                    } else {
                        buttonStatus.getStyleClass().remove("buttonErrorStatusClosed");
                        buttonStatus.getStyleClass().add("buttonErrorStatusOpen");
                    }
                } else {
                    if (errorDataManager.updateStatus(error.getId(), CLOSED) != 200) {
                        ErrorController.sendAlert("Update status failed");
                        logger.error("Update Status of error " + error.getId() + " to CLOSED failed");
                    } else {
                        buttonStatus.getStyleClass().remove("buttonErrorStatusOpen");
                        buttonStatus.getStyleClass().add("buttonErrorStatusClosed");
                    }
                }
                buttonStatus.setText(error.getStatus().name());
            } catch (Exception e) {
                ErrorController.sendAlert(e.toString());
                logger.error("Update status of tickets failed", e);
            }
        });
    }


    void setRouter(final Router router){
        this.router = router;
    }

    @FXML
    public void goToAnalytics(){
        try {
            this.router.<AnalyticsController>goTo("Analytics", controller -> {
                controller.setRouter(router);
            });
        } catch (Exception e) {
            ErrorController.sendAlert(e.toString());
            logger.error("Routing to Analytics failed", e);
        }
    }
    @FXML
    public void goToErrorTickets(){
        try {
            this.router.<ErrorTicketsController>goTo("ErrorTickets", controller -> {
                controller.setRouter(router);
            });
        } catch (Exception e) {
            ErrorController.sendAlert(e.toString());
            logger.error("Routing to ErrorTickets failed", e);
        }
    }
}
