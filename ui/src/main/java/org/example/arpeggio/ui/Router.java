package org.example.arpeggio.ui;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.function.Consumer;

public class Router {

    private final Stage stage;

    public Router (final Stage stage){
        this.stage = stage;
    }

    void goTo(final String viewName) throws IOException {
        goTo(viewName, __ -> {});
    }

    public <T> void goTo(final String viewName, final Consumer<T> controllerConsumer) throws IOException {
        final var view = loadView(viewName, controllerConsumer);
        stage.setScene(new Scene(view));
    }

    private <T> Parent loadView(String viewName, Consumer<T> controllerConsumer) throws IOException {
        final var viewPath = String.format("/%sView.fxml", viewName);
        final var fxmlLoader = new FXMLLoader(this.getClass().getResource(viewPath));
        final Parent view = fxmlLoader.load();
        controllerConsumer.accept(fxmlLoader.getController());
        return view;
    }


    public Stage getStage() {
        return this.stage;
    }
}
