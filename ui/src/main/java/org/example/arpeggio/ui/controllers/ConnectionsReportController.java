package org.example.arpeggio.ui.controllers;

import javafx.fxml.FXML;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.GridPane;
import org.example.arpeggio.core.models.Connection;
import org.example.arpeggio.core.dataManagers.ConnectionDataManager;
import org.example.arpeggio.ui.Router;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.*;
import java.util.Map.Entry;

public class ConnectionsReportController {

    private Router router;
    private ConnectionDataManager connectionManager;
    private ExportDataController exportDataController;
    private static final Logger logger = LoggerFactory.getLogger(ConnectionsReportController.class);

    @FXML
    private Button exportDataBtn;

    @FXML
    private LineChart<String, Integer> dataChart;
    @FXML
    private ScrollPane dataGridPlace;

    @FXML
    public void initialize(){
        try {
            this.exportDataController = new ExportDataController();
            this.exportDataBtn.setOnAction(actionEvent -> {
                try {
                    this.exportDataController.start(this.connectionManager);
                } catch (Exception e) {
                    ErrorController.sendAlert(e.toString());
                    logger.error("Export failed", e);
                }
            });
        } catch (Exception e) {
            ErrorController.sendAlert(e.toString());
            logger.error("Plugins loading failed", e);
        }
    }

    public void setRouter(final Router router){
        this.router = router;
    }

    public void setData(final LocalDate startDate, final LocalDate endDate, final boolean needRegions, final String deviceChoice, final String connectionType){
        try {
            this.connectionManager = new ConnectionDataManager(startDate, endDate, needRegions, connectionType, deviceChoice);
            completeGrid(connectionManager);
            completeLineChart(connectionManager);
        } catch (Exception e) {
            ErrorController.sendAlert(e.toString());
            logger.error("init ConnectionDataManager failed", e);
        }
    }


    @FXML
    public void goToAnalytics(){
        try {
            this.router.<AnalyticsController>goTo("Analytics", controller -> {
                controller.setRouter(router);
            });
        } catch (Exception e) {
            ErrorController.sendAlert(e.toString());
            logger.error("Routing to Analytics failed", e);
        }
    }
    @FXML
    public void goToErrorTickets(){
        try {
            this.router.<ErrorTicketsController>goTo("ErrorTickets", controller -> {
                controller.setRouter(router);
            });
        } catch (Exception e) {
            ErrorController.sendAlert(e.toString());
            logger.error("Routing to ErrorTickets failed");
        }
    }

    private void completeLineChart(ConnectionDataManager connectionDataManager) {
        final NumberAxis yAxis = new NumberAxis();
        final CategoryAxis xAxis = new CategoryAxis();
        xAxis.setLabel("Date/Hours");
        yAxis.setLabel("Count");
        // We have to create the same number of series as we have regions
        for (Entry<String, LinkedHashMap<String, ArrayList<Connection>>> region: connectionDataManager.getDataChart().entrySet()){
            XYChart.Series<String, Integer> series = new XYChart.Series<String, Integer>();
            series.setName(region.getKey());
            for (Entry<String, ArrayList<Connection>> regionByDate : region.getValue().entrySet()){
                XYChart.Data<String, Integer> dataToAdd = new XYChart.Data<String, Integer>(regionByDate.getKey(), regionByDate.getValue().size());
                series.getData().add(dataToAdd);
            }
            this.dataChart.getData().add(series);
        }
    }

    private void completeGrid(ConnectionDataManager connectionDataManager){
        GridPane dataGrid = new GridPane();
        //init first row
        Label col0Name = new Label(connectionDataManager.getOnePeriodUnit());
        dataGrid.add(col0Name, 0, 0, 1,1);
        Label col1Name = new Label("Number of connections");
        dataGrid.add(col1Name, 1, 0, connectionDataManager.getNbRegions(),1);

        //complete
        int x = 0;
        int y = 1;
        for (Entry<String,LinkedHashMap<String, ArrayList<Connection>>> mapEntry : connectionDataManager.getDataGrid().entrySet()) {
            dataGrid.add(new Label(mapEntry.getKey()), x, y, 1, 2);
            x++;
            for (String key : mapEntry.getValue().keySet()){
                dataGrid.add(new Label(key), x, y, 1, 1);
                dataGrid.add(new Label(Integer.toString(mapEntry.getValue().get(key).size())), x, y+1, 1, 1);
                x ++;
            }
            y += 2;
            x = 0;
        }

        dataGrid.getStyleClass().add("grid");

        // place in scene
        this.dataGridPlace.setContent(dataGrid);
    }

}
