package org.example.arpeggio.ui.controllers;

import javafx.fxml.FXML;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import org.example.arpeggio.core.dataManagers.CreationsDataManager;
import org.example.arpeggio.ui.Router;
import org.example.arpeggio.ui.services.CreationsReportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;

import static javafx.scene.paint.Color.RED;

public class AnalyticsController {

    private Router router;
    private CreationsReportService creationsReportService;
    private static final Logger logger = LoggerFactory.getLogger(AnalyticsController.class);


    // Form for connection's Report
    @FXML
    private DatePicker connectionsStartDate;
    @FXML
    private DatePicker connectionsEndDate;
    @FXML
    private MenuButton connectionsDevice;
    @FXML
    private MenuButton connectionsType;
    @FXML
    private CheckBox connectionsDetailRegions;
    @FXML
    private Label errorConnectionForm;

    // Objects for creations stats
    @FXML
    private MenuButton creationsPeriod;
    @FXML
    private LineChart<String, Integer> creationsLineChart;

    @FXML
    public void initialize(){
        connectionsDevice.getItems().forEach(item -> {
            item.setOnAction(actionEvent -> {
                connectionsDevice.setText(item.getText());
            });
        });
        connectionsType.getItems().forEach(item -> {
            item.setOnAction(actionEvent -> {
                connectionsType.setText(item.getText());
            });
        });

        this.creationsReportService = new CreationsReportService(creationsPeriod.textProperty());
        this.creationsReportService.setOnSucceeded(event -> {
          this.initLineChart((CreationsDataManager) event.getSource().getValue());
        });
        this.creationsReportService.setOnFailed(event -> {
            ErrorController.sendAlert(event.getSource().getException().toString());
            logger.error("Error creationsReportService", event.getSource().getException());
        });
        this.creationsPeriod.getItems().forEach(item -> {
            item.setOnAction(actionEvent -> {
                this.creationsReportService.reset();
                this.creationsPeriod.setText(item.getText());
                this.creationsReportService.start();
            });
        });
        this.creationsPeriod.setText("Last year");
        this.creationsReportService.start();
    }

    private void initLineChart(CreationsDataManager creationsDataManager) {
        this.creationsLineChart.getData().clear();
        final NumberAxis yAxis = new NumberAxis();
        final CategoryAxis xAxis = new CategoryAxis();
        xAxis.setLabel("Date/Hours");
        yAxis.setLabel("Count");
        XYChart.Series<String, Integer> series = new XYChart.Series<>();
        series.setName("Number of creations");
        for (Map.Entry<String, Integer> creationEntry : creationsDataManager.getCreationsByDate().entrySet()){
            XYChart.Data<String, Integer> dataToAdd = new XYChart.Data<>(creationEntry.getKey(), creationEntry.getValue());
            series.getData().add(dataToAdd);
        }
        this.creationsLineChart.getData().add(series);
    }

    public void updateMenuButton(MenuButton mb, String text){
        mb.setText(text);
    }

    @FXML
    public void updateConnectionTypeResponse(){
        updateMenuButton(this.connectionsType,"Response");
    }

    @FXML
    public void updateConnectionTypeRequest(){
        updateMenuButton(this.connectionsType,"Request");
    }


    @FXML
    public void goToErrorTickets() {
        try {
            this.router.<ErrorTicketsController>goTo("ErrorTickets", controller -> {
                controller.setRouter(router);
            });
        } catch (IOException e) {
            ErrorController.sendAlert(e.toString());
            logger.error("Routing to ErrorTickets failed", e);
        }
    }

    @FXML
    public void runConnectionsReport(){ // we don't use tasks because we want to block the user during the change of page
        if (isValid()){
            try {
                this.router.<ConnectionsReportController>goTo("ConnectionsReport", controller -> {
                        controller.setRouter(router);
                        controller.setData(connectionsStartDate.getValue(), connectionsEndDate.getValue(),
                                connectionsDetailRegions.isSelected(), connectionsDevice.getText(), connectionsType.getText());
                });
            } catch (IOException e) {
                ErrorController.sendAlert(e.toString());
                logger.error("Routing to ConnectionsReport failed", e);
            }
        } else {
            errorConnectionForm.setTextFill(RED);
            errorConnectionForm.setText("The form is invalid");
        }
    }

    public void setRouter(final Router router){
        this.router = router;
    }

    private boolean isValid(){
        if (connectionsDevice.getText().equals("Device")) {
            return false;
        }
        if (connectionsType.getText().equals("Connection Type")) {
            return false;
        }
        if (connectionsStartDate.getValue() == null || connectionsEndDate.getValue() == null){
            return false;
        }
        if (connectionsStartDate.getValue().isAfter(connectionsEndDate.getValue())){
            return false;
        }
        return true;
    }

}
