package org.example.arpeggio.ui;

import ch.qos.logback.classic.util.ContextInitializer;
import javafx.application.Application;
import javafx.stage.Stage;
import org.example.arpeggio.ui.controllers.AnalyticsController;
import org.example.arpeggio.ui.controllers.ErrorController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MainApplication extends Application {
    private static final Logger logger = LoggerFactory.getLogger(MainApplication.class);

    @Override
    public void start(Stage stage) {
        logger.info("java version: " + System.getProperty("java.version"));
        logger.info("javafx.version: " + System.getProperty("javafx.version"));

        final var router = new Router(stage);
        try {
            router.<AnalyticsController>goTo("Analytics", controller -> {
                controller.setRouter(router);
            });
        } catch (Exception e) {
            ErrorController.sendAlert(e.toString());
            logger.error("Routing to Analytics failed");
        }
        stage.setTitle("Arpeggio");
        stage.setMinHeight(850);
        stage.setMinWidth(1400);
        stage.setMaxHeight(850);
        stage.setMaxHeight(1400);
        stage.setWidth(850);
        stage.setHeight(1400);
        stage.show();
    }

    public static void main(String[] args) {
        logger.info("Application JFX started");
        launch(args);
        logger.info("Application JFX ended");
    }
}
