package org.example.arpeggio.ui.services;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import org.example.arpeggio.core.dataManagers.ErrorDataManager;
import org.example.arpeggio.ui.controllers.ErrorController;
import org.example.arpeggio.ui.tasks.ErrorDataTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;

public class ErrorTicketsService extends Service<ErrorDataManager> {

    private final StringProperty status;
    private final ObjectProperty<LocalDate> dateOfTickets;
    private static final Logger logger = LoggerFactory.getLogger(ErrorTicketsService.class);


    public ErrorTicketsService(StringProperty status, ObjectProperty<LocalDate> dateOfTickets) {
        this.status = status;
        this.dateOfTickets = dateOfTickets;
    }

    @Override
    protected Task<ErrorDataManager> createTask() {
        try {
            return new ErrorDataTask(status.getValueSafe(), dateOfTickets.getValue());
        } catch (Exception e) {
            ErrorController.sendAlert(e.getMessage());
            logger.error("ErrorDataTask initialization failed", e);
        }
        return null;
    }
}
