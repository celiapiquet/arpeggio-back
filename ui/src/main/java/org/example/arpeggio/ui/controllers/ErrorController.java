package org.example.arpeggio.ui.controllers;

import javafx.scene.control.Alert;

public class ErrorController {

    public static void sendAlert(String stacktrace){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("Please contact the support and give the following :");
        alert.setContentText(stacktrace);
        alert.showAndWait();
    }
}
