package org.example.arpeggio.ui.tasks;

import javafx.concurrent.Task;
import org.example.arpeggio.core.dataManagers.CreationsDataManager;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class CreationsDataTask extends Task<CreationsDataManager> {

    private final LocalDate startDate;
    private final LocalDate endDate;
    private final LocalDate today;
    private ChronoUnit unitUsed;

    public CreationsDataTask(final String period) throws Exception {
        this.today = LocalDate.now();
        this.startDate = getStartDateFromPeriod(period);
        this.endDate = getEndDateFromPeriod(period); //excluded by the api
    }

    private LocalDate getEndDateFromPeriod(String period) {
        if (period.equals("Yesterday")){
            return today;
        }
        return today.plus(1, ChronoUnit.DAYS);
    }

    private LocalDate getStartDateFromPeriod(String period) throws Exception {
        if (period.equals("Today")){
            unitUsed = ChronoUnit.HOURS;
            return today;
        }
        if (period.equals("Yesterday")){
            unitUsed = ChronoUnit.HOURS;
            return today.minus(1, ChronoUnit.DAYS);
        }
        if (period.equals("Last 7 days")){
            unitUsed = ChronoUnit.DAYS;
            return today.minus(1, ChronoUnit.WEEKS);
        }
        if (period.equals("Last month")){
            unitUsed = ChronoUnit.DAYS;
            return today.minus(1, ChronoUnit.MONTHS);
        }
        if (period.equals("Last year")){
            unitUsed = ChronoUnit.MONTHS;
            return today.minus(1, ChronoUnit.YEARS);
        }
        throw new Exception("Invalid period");
    }

    @Override
    protected CreationsDataManager call() throws Exception {
        return new CreationsDataManager(startDate, endDate, unitUsed);
    }
}
