package org.example.arpeggio.ui.tasks;

import javafx.concurrent.Task;
import org.example.arpeggio.core.enums.ErrorStatus;
import org.example.arpeggio.core.dataManagers.ErrorDataManager;

import java.time.LocalDate;

public class ErrorDataTask extends Task<ErrorDataManager> {

    private final ErrorStatus status;
    private final LocalDate dateOfTickets;

    public ErrorDataTask(final String status, final LocalDate dateOfTickets) throws Exception{
        if (ErrorStatus.hasValue(status)){
            this.status = ErrorStatus.valueOf(status);
        } else {
            this.status = null;
        }
        this.dateOfTickets = dateOfTickets;
    }

    @Override
    protected ErrorDataManager call() throws Exception {
        return new ErrorDataManager(status, dateOfTickets);
    }
}
