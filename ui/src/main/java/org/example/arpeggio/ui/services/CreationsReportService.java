package org.example.arpeggio.ui.services;

import javafx.beans.property.StringProperty;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import org.example.arpeggio.core.PluginManagement.PluginManager;
import org.example.arpeggio.core.dataManagers.CreationsDataManager;
import org.example.arpeggio.di.annotations.Inject;
import org.example.arpeggio.ui.controllers.ErrorController;
import org.example.arpeggio.ui.tasks.CreationsDataTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreationsReportService extends Service<CreationsDataManager> {

    private final StringProperty period;
    private static final Logger logger = LoggerFactory.getLogger(CreationsReportService.class);

    public CreationsReportService(final StringProperty period){
        this.period = period;
    }

    @Override
    protected Task<CreationsDataManager> createTask() {
        try {
            return new CreationsDataTask(period.getValueSafe());
        } catch (Exception e) {
            ErrorController.sendAlert(e.toString());
            logger.error("CreationsDataTask initialization failed", e);
        }
        return null;
    }
}
