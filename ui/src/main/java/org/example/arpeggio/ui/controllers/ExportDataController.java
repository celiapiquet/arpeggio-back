package org.example.arpeggio.ui.controllers;

import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceDialog;
import org.example.arpeggio.core.PluginManagement.PluginManager;
import org.example.arpeggio.core.dataManagers.ConnectionDataManager;
import org.example.arpeggio.plugins.ExporterContract;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Optional;

public class ExportDataController {

    private final PluginManager<ExporterContract> pluginManager;
    private static final Logger logger = LoggerFactory.getLogger(ExportDataController.class);


    public ExportDataController() throws Exception {
        this.pluginManager = new PluginManager<>();
        pluginManager.loadPluginsClass();
    }

    public void start(ConnectionDataManager connectionDataManager) throws Exception {

        // Get all plugins installed
        ArrayList<ExporterContract> formatsAvailable = pluginManager.getPluginsInstance();

        if (formatsAvailable.size() == 0){
            this.noPluginAvailableDialog();
            return;
        }
        // Propose all format proposed by plugins
        ChoiceDialog<ExporterContract> exportModal = createNewModal(formatsAvailable);
        Optional<ExporterContract> result = exportModal.showAndWait();

        // Start action when needed
        result.ifPresent(exporterContract -> {
            try {
                final JSONArray data = new JSONArray(connectionDataManager.getConnections());
                final String title = connectionDataManager.toString();
                exporterContract.uploadFile(data, title);
            } catch (Exception e) {
                logger.error("Upload file not possible", e);
            }
        });

    }

    private void noPluginAvailableDialog() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("No plugins detected");
        alert.setHeaderText("Please make sure to download some plugins to use this feature.");
        alert.showAndWait();
    }

    private ChoiceDialog<ExporterContract> createNewModal(ArrayList<ExporterContract> formatsAvailable) {
        final ChoiceDialog<ExporterContract> modal = new ChoiceDialog<>(formatsAvailable.get(0), formatsAvailable);
        modal.setTitle("Export Data");
        modal.setHeaderText("Please choose one format. Download the nested plugins if you don't have one.");
        return modal;
    }
}
