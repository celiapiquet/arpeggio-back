package org.example.arpeggio.plugins;

import org.json.JSONArray;

public abstract class ExporterContract {

    public abstract void uploadFile(JSONArray data, String title) throws Exception;

    @Override
    public abstract String toString();
}
