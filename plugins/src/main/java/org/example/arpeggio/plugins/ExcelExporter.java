package org.example.arpeggio.plugins;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;


public class ExcelExporter extends ExporterContract {

    public void uploadFile(JSONArray data, String title) throws Exception {
        FileOutputStream exportFile = initFileOutput();
        XSSFWorkbook wb = completeWorkBook(data, title);
        wb.write(exportFile);
        exportFile.close();

    }

    private XSSFWorkbook completeWorkBook(JSONArray data, String title) {
        // create empty file : workbook
        XSSFWorkbook wb = new XSSFWorkbook();

        // crate empty sheet
        Sheet sheet = wb.createSheet("Arpeggio_sheet1");

        //create line
        Row row = sheet.createRow(0);

        // create cell
        Cell cell = row.createCell(0);
        cell.setCellValue(title);
        for (int i=0; i < data.length(); i++){
            JSONObject jsonObject = (JSONObject) data.get(i);
            row = sheet.createRow(i+1);
            cell = row.createCell(0);
            cell.setCellValue(jsonObject.toString());
        }
        return wb;
    }

    private FileOutputStream initFileOutput() throws Exception {
        LocalDateTime localDateTime = LocalDateTime.now();
        String time = localDateTime.toLocalTime().truncatedTo(ChronoUnit.SECONDS).toString()+"s";
        time = time.replaceFirst(":","h");
        time = time.replaceFirst(":","m");

        String date = localDateTime.toLocalDate().toString();

        String filename = "report_"+date+"_"+time;

        return new FileOutputStream(System.getProperty("user.dir") + "/" + filename+".xlsx");
    }

    @Override
    public String toString() {
        return "Excel";
    }
}
