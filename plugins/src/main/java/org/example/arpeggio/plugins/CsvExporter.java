package org.example.arpeggio.plugins;

import org.json.CDL;
import org.json.JSONArray;


import java.io.FileOutputStream;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class CsvExporter extends ExporterContract{

    @Override
    public void uploadFile(JSONArray data, String title) throws Exception {
        LocalDateTime localDateTime = LocalDateTime.now();
        String time = localDateTime.toLocalTime().truncatedTo(ChronoUnit.SECONDS).toString()+"s";
        time = time.replaceFirst(":","h");
        time = time.replaceFirst(":","m");

        String date = localDateTime.toLocalDate().toString();

        String filename = "report_"+date+"_"+time;
        FileOutputStream file = new FileOutputStream(filename + ".csv");

        String csv = CDL.toString(data);
        file.write(csv.getBytes());
        file.close();

    }

    @Override
    public String toString() {
        return "CSV";
    }
}
