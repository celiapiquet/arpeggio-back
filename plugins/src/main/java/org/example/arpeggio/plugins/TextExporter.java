package org.example.arpeggio.plugins;

import org.json.JSONArray;

import java.io.FileOutputStream;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class TextExporter extends ExporterContract {

    public void uploadFile(JSONArray data, String title) throws Exception {

        LocalDateTime localDateTime = LocalDateTime.now();
        String time = localDateTime.toLocalTime().truncatedTo(ChronoUnit.SECONDS).toString()+"s";
        time = time.replaceFirst(":","h");
        time = time.replaceFirst(":","m");

        String date = localDateTime.toLocalDate().toString();

        String filename = "report_"+date+"_"+time;
        FileOutputStream file = new FileOutputStream(filename + ".txt");
        file.write((title + data.toString()).getBytes() );
        file.close();
    }

    @Override
    public String toString() {
        return "Text";
    }
}
