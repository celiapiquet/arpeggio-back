# Arpeggio Back

[![forthebadge](http://forthebadge.com/images/badges/made-with-java.svg)](http://forthebadge.com)  [![forthebadge](http://forthebadge.com/images/badges/powered-by-electricity.svg)](http://forthebadge.com)

Arpeggio Back est une application lourde développée en Java et utilisant JavaFX, LogBack et Jackson. 
Elle permet de récupérer des statistiques du site web de même nom ainsi que le suivi des erreurs de l'API.
Composée de plusieurs modules elle comprend entre autre un moteur d'injection de dépendance, une gestion de plugins
et une interface graphique.

## Utilisation

Pour UTILISER l'application récupérer directement le repertoire readyToRun. Il comprend 2 jar exécutables en
fonction de l'utilisation souhaitée ainsi que le répertoire avec les différents plugins. Si un plugin doit être ajouté
cela doit se faire dans plugins/target. Un fichier de log sera automatiquement créé lors de l'utilisation de arpeggio-ui,
il permet notamment d'avoir une trace en cas d'erreurs.
```
readyToRun/
├── arpeggio-cli.jar
├── arpeggio-ui.jar
|--- arpeggio.log (pas présent par défaut)
├── plugins/
│   ├── target/
│       ├── exporterCsv.jar
│       ├── exporterTxt.jar
│       └── exporterExcel.jar
```
## Contribution

Pour CONTRIBUER, vous pouvez récupérer le projet en local : ``git clone https://gitlab.com/celiapiquet/arpeggio-back.git``
Attention à mettre à jour votre version de Java dans les pom.xml

## Fabriqué avec

* [IntelliJ IDEA](https://www.jetbrains.com/fr-fr/idea/) - IDE Java
* [Scene Builder](https://gluonhq.com/products/scene-builder/) - Conception d'interface graphique pour JavaFX

## Versions
**Dernière version stable :** 1.0.0
